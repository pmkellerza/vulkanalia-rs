use super::prelude::*;

pub const VALIDATION_ENABLED: bool = cfg!(debug_assertions);

pub const VALIDATION_LAYER: vk::ExtensionName =
    vk::ExtensionName::from_bytes(b"VK_LAYER_KHRONOS_validation");

pub const PORTABILITY_MACOS_VERSION: Version = Version::new(1, 3, 216);
pub const DEVICE_EXTENSIONS: &[vk::ExtensionName] = &[vk::KHR_SWAPCHAIN_EXTENSION.name];
pub const WINDOW_TITLE: &str = "Vulkan Tutorial (Rust)";
