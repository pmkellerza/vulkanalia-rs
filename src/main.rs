#![allow(
    dead_code,
    unused_variables,
    clippy::too_many_arguments,
    clippy::unnecessary_wraps
)]

use std::time::{Duration, Instant};

mod constants;
mod debug;
mod prelude;
mod vkframebuffer;
mod vkinstance;
mod vkphysicaldevice;
mod vkpiplines;
mod vkqueuefamilies;
mod vkswapchains;
mod vulkanapp;

use prelude::*;

fn main() -> Result<()> {
    pretty_env_logger::init();

    //Create winit window and event looop

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title(WINDOW_TITLE)
        .with_inner_size(LogicalSize::new(1024, 768))
        .build(&event_loop)?;

    info!("Winit Window Created");

    // Initialize FPS tracking variables
    let mut frame_count = 0;
    let mut last_frame_time = Instant::now();
    let mut fps = 0.0;

    //Create vulkan app
    let mut app = unsafe { VkApp::create(&window)? };
    let mut destorying = false;

    //Main Event Loop
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            // Render a frame if our vulkan app is not being destoryed
            Event::MainEventsCleared if !destorying => unsafe {
                #[cfg(feature = "fps")]
                {
                    // Calculate FPS
                    frame_count += 1;
                    let current_time = Instant::now();
                    let elapsed_time = current_time.duration_since(last_frame_time);
                    if elapsed_time >= Duration::from_secs(1) {
                        fps = frame_count as f64 / elapsed_time.as_secs_f64();
                        frame_count = 0;
                        last_frame_time = current_time;

                        // Update the window title with FPS
                        window.set_title(format!("{} - FPS: {:.2}", WINDOW_TITLE, fps).as_str());
                    }
                }

                app.render(&window).unwrap();
            },

            // Destory our Vulkan app
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                destorying = true;
                *control_flow = ControlFlow::Exit;
                unsafe { app.destory() }
            }
            _ => {}
        }
    });
}
